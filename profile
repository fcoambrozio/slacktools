# Slackware settings

export SLKBUILDS="/var/slackware/slackbuilds/builds"
export PKGS="/var/slackware/slackbuilds/packages"

export PKGTYPE="txz"
export NUMJOBS="-j4"

sbo_prepare() {
    if [ ! -z $1 ]; then
        tar vxf ${1}.tar.gz || return 1
        cd ${1}
        . ${1}.info
        wget -c $DOWNLOAD || return 1
        cd -
        rm -f ${1}.tar.gz
    fi
}


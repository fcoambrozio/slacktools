#!/bin/bash
#
# Verify broken shared libs 
#
# Francisco Ambrozio Wed Oct 16 15:02:46 BRT 2013
#

#
# TODO: Check all packages in /var/log/packages, for now excepts pkgname to 
#  be passed as an argument.
#

if [ -z $1 ]; then
    echo "Package name argument required."
    exit 1
fi

PKGDB="/var/log/packages"

if ! ls ${PKGDB}/${1}* &>/dev/null ; then
    echo "Package $1 not found"
    exit 1
fi

PKGS=$(ls ${PKGDB}/${1}* | cut -f5 -d"/")

for pkg in $PKGS
do
    OUTPUT=$(mktemp)
    echo "======================================================================"
    echo "+++ Verifying package: $pkg"
    echo "======================================================================"

    for lib in $(egrep '*.so$' ${PKGDB}/${pkg})
    do
        ldd /${lib} | grep "not found" | awk '{print $1}' >> $OUTPUT
    done

    if [ -s $OUTPUT ]; then
        echo "!!! Broken libs found:"
        for broken in $(cat $OUTPUT | uniq)
        do
            echo "       --> $broken"
        done
    else
        echo "--- No broken libs found"
    fi

    rm $OUTPUT
done


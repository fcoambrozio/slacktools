#!/bin/bash
#
# local_mirror.sh
#
# Sync up Slackware-current local mirror
#
# Francisco Ambrozio - 2013-10-28
#
#
### ChangeLog #############################################
#
# Wed Apr 13 08:53:37 BRT 2011
#  Cloned 32 bits repository
#----------------------------------------------------------
# Sat Apr 16 13:44:11 BRT 2011
#  SLKARCH now default to 64
#----------------------------------------------------------
# Mon Jun 13 22:47:04 BRT 2011
#  LOCALMIRROR moved to a new place
#  Added option to generate a iso
#----------------------------------------------------------
# Tue Jun 14 08:46:09 BRT 2011
#  Fixed a little bug in ISO-gen
#----------------------------------------------------------
# Fri Jun 24 22:05:58 BRT 2011
# LOCALMIRROR moved to a new place (again :))
#----------------------------------------------------------
# Wed Apr 11 19:11:53 BRT 2012
# Now with configuration file - no more editing the script
# to move LOCALMIRROR of place :)
#----------------------------------------------------------
# Sat Sep  1 15:48:10 BRT 2012
# Append timestamp to ISO filename
#----------------------------------------------------------
# Mon Oct 28 10:18:46 BRST 2013
# New name and new house
#
###########################################################
#

CONFIGFILE="${HOME}/.mirror-slack-local.rc"

#
# Keep this, just in case :)
#
#SLKARCH="64"
#MIRRORDIR="/data/slackware/mirrors"
#ISODIR="/data/ISOs"
#DOISO="0"
#RSYNCMIRROR="rsync://slackware.oregonstate.edu/slackware/slackware${SLKARCH}-current/"
#RSYNCOPTS=" -avzP --delete --exclude=source "
#

create_config()
{
	echo "Create $(basename $0) configuration file: ${CONFIGFILE}."
	read -p "Enter local mirror directory: " dir
	read -p "Enter ISO destination directory: " iso
	echo "Default option [these can be changed in runtime using parameters]"
	read -p "Arch: " arch
	read -p "Generate ISO file [0] No - [1] Yes: " giso
	if [ ${giso} != "0" -a ${giso} != "1" ] ; then
		echo "Unknow option ${giso}, assuming No"
		giso=0
	fi

	echo "SLKARCH=\"${arch}\"" > $CONFIGFILE
	echo "MIRRORDIR=\"${dir}\"" >> $CONFIGFILE
	echo "ISODIR=\"${iso}\"" >> $CONFIGFILE
	echo "DOISO=\"${giso}\"" >> $CONFIGFILE
	echo "RSYNCMIRROR=\"rsync://slackware.oregonstate.edu/slackware/slackware@ARCH@-current/\"" >> $CONFIGFILE
	echo "RSYNCOPTS=\" -avzP --delete --exclude=source \"" >> $CONFIGFILE
}

test_param()
{
	case "$1" in
		"32")
			SLKARCH=""
			;;
		"--iso")
			DOISO="1"
			;;
		*)
			echo "Unknow parameter $1. Using default options."
			echo ""
	esac
}

if [ ! -f $CONFIGFILE ]; then
	echo "No configuration file found."
	create_config
fi

. $CONFIGFILE

[ -n "$1" ] && test_param $1
[ -n "$2" ] && test_param $2

if [ -z "$MIRRORDIR" -o -z "$ISODIR" -o -z "$DOISO" -o -z "$RSYNCMIRROR" -o -z "$RSYNCOPTS" ] ; then
	exit 1
fi

RSYNCMIRROR=$(echo "$RSYNCMIRROR" | sed "s/@ARCH@/$SLKARCH/")
LOCALMIRROR="${MIRRORDIR}/slackware${SLKARCH}-current"

[ -d $LOCALMIRROR ] || mkdir -p $LOCALMIRROR

cd $LOCALMIRROR || exit 1
/usr/bin/rsync $RSYNCOPTS $RSYNCMIRROR .

if [ "$DOISO" == "1" ]; then
	ISOOUTPUT="$ISODIR/slackware${SLKARCH}-current-install-dvd_$(date +%Y%m%d%H%M).iso"
	[ -d $ISODIR ] || mkdir -p $ISODIR
	/usr/bin/mkisofs -o $ISOOUTPUT \
		-R -J -V "Slackware${SLKARCH}-current DVD" \
		-hide-rr-moved -hide-joliet-trans-tbl \
		-v -d -N -no-emul-boot -boot-load-size 4 -boot-info-table \
		-x ./testing \
		-sort isolinux/iso.sort \
		-b isolinux/isolinux.bin \
		-c isolinux/isolinux.boot \
		-publisher "The Slackware Linux Project - http://www.slackware.com/" \
		-A "Slackware${SLKARCH}-current DVD - build $(date +%d_%b_%Y)" .
fi

